from django.shortcuts import render, redirect
from .forms import log_in, sign_up
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User


# Create your views here.
def user_login(request):
    if request.method == "POST":
        form = log_in(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("home")
    else:
        form = log_in()
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


# once logged in take me to home page


def user_logout(request):
    logout(request)
    return redirect("login")


def new_sign_up(request):
    if request.method == "POST":
        form = sign_up(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            confirm = form.cleaned_data["password_confirmation"]
            if password == confirm:
                user = User.objects.create_user(username, password=password)
                login(request, user)
                return redirect("home")
            else:
                form.add_error("password", "the passwords do not match")
    else:
        form = sign_up()
    context = {"form": form}
    return render(request, "accounts/signup.html", context)
